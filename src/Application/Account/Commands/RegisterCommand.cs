﻿using MediatR;
using Microsoft.Extensions.Logging;
using MLM.Application.Common.Interfaces;
using MLM.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MLM.Application.Account.Commands
{
    public class RegisterCommand : IRequest
    {
        public SponcerPackage sponcerPackage { get; set; }
        public User user { get; set; }
        public Login login { get; set; }
        //public Payment payment { get; set; }
        public class SponcerPackage 
        {
            public string SponsorCode { get; set; }
            public int PackageId { get; set; }
        }
        public class User 
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public DateTime Dob { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
        }
        public class Login
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
        //public class Payment
        //{
        //    public string Razorpay_payment_id { get; set; }
        //    public string Razorpay_order_id { get; set; }
        //    public string Razorpay_signature { get; set; }
        //}
    }

    public class RegisterCommandHandler : IRequestHandler<RegisterCommand>
    {
        private readonly IIdentityService identityService;
        private readonly IApplicationDbContext context;
        private readonly ILogger<RegisterCommandHandler> logger;
        private readonly ICurrentUserService currentUserService;

        public RegisterCommandHandler(IIdentityService identityService,IApplicationDbContext context,ILogger<RegisterCommandHandler> logger,ICurrentUserService currentUserService)
        {
            this.identityService = identityService;
            this.context = context;
            this.logger = logger;
            this.currentUserService = currentUserService;
        }
        public async Task<Unit> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            try
            {
                //await razorPayServices.CheckSignature(request.payment.Razorpay_order_id, request.payment.Razorpay_payment_id, request.payment.Razorpay_signature);
                var identityResult = await identityService.CreateUserAsync(request.login.UserName, request.login.Password, request.user.Email, request.user.PhoneNumber);
                if (!identityResult.Result.Succeeded)
                {
                    logger.LogError(identityResult.Result.Errors.ToString());
                    throw new Exception(identityResult.Result.Errors.ToString());
                }
                var entity = new User()
                {
                    CreatedBy=currentUserService.UserId,
                    Created = DateTime.Now,
                    Dob=request.user.Dob,
                    FirstName=request.user.FirstName,
                    LastName=request.user.LastName,
                    UserId=identityResult.UserId,
                    SponsorUsername=request.sponcerPackage.SponsorCode,
                    PackageId=request.sponcerPackage.PackageId,
                };

                context.User.Add(entity);
                await context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
            return Unit.Value;
        }
    }
}
