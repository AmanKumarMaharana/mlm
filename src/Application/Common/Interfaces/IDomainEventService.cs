﻿using MLM.Domain.Common;
using System.Threading.Tasks;

namespace MLM.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
