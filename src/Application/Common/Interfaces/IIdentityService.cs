﻿using MLM.Application.Common.Models;
using System.Threading.Tasks;

namespace MLM.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(string userId);

        Task<bool> IsInRoleAsync(string userId, string role);

        Task<bool> AuthorizeAsync(string userId, string policyName);

        Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password, string email, string phone);

        Task<Result> DeleteUserAsync(string userId);
    }
}
