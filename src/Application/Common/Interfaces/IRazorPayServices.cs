﻿using System.Threading.Tasks;

namespace MLM.Application.Common.Interfaces
{
    public interface IRazorPayServices
    {
        Task<string> CreateCustomer(string name, string contact);
        Task<(dynamic orderId, string razorPayKey)> CreateOrder(int amount);
        Task CheckSignature(string razorpay_order_id, string razorpay_payment_id, string razorpay_signature);
        Task PaymentCaptured(string amount, string razorpay_payment_id);

    }
}
