﻿using MLM.Application.Common.Mappings;
using MLM.Domain.Entities;

namespace MLM.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
