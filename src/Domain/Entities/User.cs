﻿using MLM.Domain.Common;
using System;

namespace MLM.Domain.Entities
{
    public class User:AuditableEntity
    {
        public long Id { get; set; }
        public string  UserId { get; set; }
        public string SponsorUsername { get; set; }
        public int PackageId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
    }
}
