﻿using Microsoft.AspNetCore.Identity;

namespace MLM.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
