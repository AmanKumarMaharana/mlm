﻿namespace MLM.Infrastructure.Model
{
    public class RazorPayOptions
    {
        public string KeyId { get; set; }
        public string KeySecret { get; set; }
    }
}
