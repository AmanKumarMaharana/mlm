﻿using MLM.Application.Common.Interfaces;
using System;

namespace MLM.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
