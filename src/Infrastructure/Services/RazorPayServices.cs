﻿using Microsoft.Extensions.Options;
using MLM.Application.Common.Interfaces;
using MLM.Infrastructure.Model;
using Razorpay.Api;
using Razorpay.Api.Errors;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MLM.Infrastructure.Services
{
    public class RazorPayServices : IRazorPayServices
    {
        private readonly RazorPayOptions _options;
        private RazorpayClient RazorpayClient { get; set; }
        public RazorPayServices(IOptions<RazorPayOptions> options)
        {
            _options = options.Value;
            RazorpayClient = new RazorpayClient(_options.KeyId, _options.KeySecret);

        }
        public Task<string> CreateCustomer(string name, string contact)
        {
            var customer = new Customer();
            try
            {
                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("name", name);
                options.Add("contact", contact);
                customer = RazorpayClient.Customer.Create(options);
            }
            catch (BadRequestError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customer["id"].ToString();
        }

        public async Task<(dynamic orderId, string razorPayKey)> CreateOrder(int amount)
        {
            var order = new Order();
            try
            {
                string transactionId = Guid.NewGuid().ToString("N");

                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", amount * 100);
                options.Add("receipt", transactionId);
                options.Add("currency", "INR");
                options.Add("payment_capture", "0");

                order = RazorpayClient.Order.Create(options);
            }
            catch (BadRequestError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (SignatureVerificationError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return (order["id"].ToString(), _options.KeyId);
        }

        public async Task CheckSignature(string razorpay_order_id, string razorpay_payment_id, string razorpay_signature)
        {
            try
            {
                var payload = razorpay_order_id + '|' + razorpay_payment_id;
                Utils.verifyWebhookSignature(payload, razorpay_signature, _options.KeySecret);
            }
            catch (BadRequestError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (SignatureVerificationError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task PaymentCaptured(string amount, string razorpay_payment_id)
        {
            try
            {
                Payment payment = RazorpayClient.Payment.Fetch(razorpay_payment_id);
                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", amount);
                options.Add("currency", "INR");
                Payment paymentCaptured = payment.Capture(options);
            }
            catch (BadRequestError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (SignatureVerificationError ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
