﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MLM.Application.Account.Commands;
using System.Threading.Tasks;

namespace MLM.WebUI.Controllers
{
    public class AccountController : ApiControllerBase
    {
        [HttpPost("[action]")]
        public async Task<IActionResult> RegisterAsync(RegisterCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
